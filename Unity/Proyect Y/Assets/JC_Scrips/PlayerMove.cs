using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMove : MonoBehaviour
{
    [SerializeField]
    private float speed;

    [SerializeField]
    private Vector3 pos1, pos2, pos3;

    private bool isPos1, isPos2, isPos3, isMoving;

    [SerializeField]
    private bool pW1, pW2;

    [SerializeField]
    private Material weapon1, weapon2, weapon3;
    private int weapon;

    [SerializeField]
    private GameManager gManager;

    void Start()
    {
        weapon = 1;
        isPos1 = false;
        isPos2 = true;
        isPos3 = false;
        isMoving = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q)){
            if(weapon == 1){
                gameObject.GetComponent<MeshRenderer>().material = weapon2;
                weapon = 2;
            }
            else if(weapon == 2){
                gameObject.GetComponent<MeshRenderer>().material = weapon3;
                weapon = 3;
            }
            else if(weapon == 3){
                gameObject.GetComponent<MeshRenderer>().material = weapon1;
                weapon = 1;
            }
        }

        if(Input.GetKeyDown(KeyCode.A) && !isMoving){
            print("A");

            if(isPos2){
                isMoving = true;
                isPos2 = false;
                isPos1 = true;
                StartCoroutine(stopMove());
            }
            else if(isPos3){
                isMoving = true;
                isPos3 = false;
                isPos2 = true;
                StartCoroutine(stopMove());
            }
        }

        if(Input.GetKeyDown(KeyCode.D) && !isMoving){

            if(isPos2){
                isMoving = true;
                isPos2 = false;
                isPos3 = true;
                StartCoroutine(stopMove());
            }
            else if(isPos1){
                isMoving = true;
                isPos1 = false;
                isPos2 = true;
                StartCoroutine(stopMove());
            }

            print("B");
        }

        if(isMoving){
            if(isPos1){
                transform.position = Vector3.MoveTowards(transform.position, pos1, speed*Time.deltaTime);
            }
            else if(isPos2){
                transform.position = Vector3.MoveTowards(transform.position, pos2, speed*Time.deltaTime);
            }
            else{
                transform.position = Vector3.MoveTowards(transform.position, pos3, speed*Time.deltaTime);
            }
        }
    }

    private IEnumerator stopMove(){
        yield return new WaitForSeconds(0.2f);
        isMoving = false;
    }

    private IEnumerator DisablePW(int pWNum){
        yield return new WaitForSeconds(3f);

        if(pWNum == 1) pW1 = false;
        else if(pWNum == 2) pW2 = false;
    }

    private void OnTriggerEnter(Collider other) {

        if(other.gameObject.CompareTag("Obstacle")){

            if(pW1){
                pW1 = false;        
            }
            else if(pW2){
            Destroy(other.gameObject);
            }
            else{
            SceneManager.LoadScene(0);
            }
        }

        if(other.gameObject.CompareTag("PowerUp1")){
            pW1 = true;
            StartCoroutine(DisablePW(1));
            Destroy(other.gameObject);
        }
        
        if(other.gameObject.CompareTag("PowerUp2")){
            pW2 = true;
            StartCoroutine(DisablePW(2));
            Destroy(other.gameObject);
        }

        if(other.gameObject.CompareTag("Food")){
            gManager.AddMultiply();
            Destroy(other.gameObject);
        }
    }
}
