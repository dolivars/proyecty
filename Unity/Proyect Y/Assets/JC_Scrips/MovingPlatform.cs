using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    float platformVelocity;
    float acceleration;
    public Vector3 direction;

    void Start()
    {
        platformVelocity = GameObject.Find("GameManager").GetComponent<GameManager>().globalVelocity;
        acceleration = GameObject.Find("GameManager").GetComponent<GameManager>().acceleration;

        direction = new Vector3(0,-platformVelocity, 0);
    }

    // Update is called once per frame
    void Update()
    {
        direction.y -= acceleration * Time.deltaTime;
        transform.Translate(direction*Time.deltaTime, Space.World);
    }
}
