using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public float globalVelocity;
    public float acceleration;

    public int multiply;
    public float score, mAmount;

    [SerializeField]
    private Text scoreTxt, multiplyTxt;
    
    void Start()
    {
        multiply = 0;
        score = 0;
        mAmount = 0;

        multiplyTxt.text = "x 0";
        scoreTxt.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        globalVelocity += acceleration*Time.deltaTime;

        score += acceleration*multiply*Time.deltaTime*10f;

        if(mAmount > 0){
            mAmount -= acceleration*Time.deltaTime*2f;
        }
        else if(mAmount < 0){
            mAmount = 0;
        }
    }

    private void LateUpdate() {
        scoreTxt.text = Mathf.FloorToInt(score).ToString();

        if(mAmount > 9){
            multiply = 4;
        }
        else if(mAmount > 6){
            multiply = 3;
        }
        else if(mAmount > 3){
            multiply = 2;
        }
        else{
            multiply = 1;
        }

        multiplyTxt.text = "x " + multiply.ToString();
    }

    public void AddMultiply(){
        if(mAmount < 10){
            mAmount++;
        }
    }
}
